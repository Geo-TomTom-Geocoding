#!perl

use Test::More tests => 1;
use Geo::TomTom::Geocoding;

SKIP: {
  my $key = $ENV{TOMTOM_API_KEY};
  skip("TOMTOM_API_KEY not set", 1) unless $key;

  my $test_loc = [
    key => $key,
    ST  => 55,
    T   => 'Music Concourse Drive',
    L   => 'San Francisco',
    AA  => 'CA',
    CC  => 'USA',
  ];
  my $response = Geo::TomTom::Geocoding->query($test_loc);
  my ($first_match) = $response->locations;
  note "Test results:\n".explain $first_match;
  ok( $first_match->{postcode} =~ /^94118/ );

}

