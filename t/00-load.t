#!perl -T

use Test::More tests => 1;

BEGIN {
    use_ok( 'Geo::TomTom::Geocoding' ) || print "Bail out!\n";
}

diag( "Testing Geo::TomTom::Geocoding $Geo::TomTom::Geocoding::VERSION, Perl $], $^X" );
