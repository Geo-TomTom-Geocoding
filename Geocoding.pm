package Geo::TomTom::Geocoding;

use 5.006;
use strict;
use warnings;

use LWP::UserAgent;
use base 'HTTP::Response';
use JSON 'decode_json';

=head1 NAME

Geo::TomTom::Geocoding - Interface to the TomTom Geocoding service

=head1 VERSION

Version 0.01

=cut

our $VERSION = '0.01';
our $DEBUG = 0;

=head1 SYNOPSIS

    use Geo::TomTom::Geocoding;

    my $request = {
      key => '12345678abcdef',# API authorization
      ST  => '123',           # address number
      T   => 'Main Street',   # street name
      L   => 'San Francisco', # locality
      AA  => 'CA',            # administrative area (state/province/district)
      PC  => '93102',         # postal code
      CC  => 'USA',           # country code (ISO three-letter)
      IS  => 'Market Street', # intersecting street
      # other options specified in the API docs
    };
    $result = Geo::TomTom::Geocoding->query($request);

    if ($result->is_success) {
      foreach ($result->locations) {
        print $_->{formattedAddress};
      }
    }

=head1 CLASS METHODS

=head2 query HASHREF

Send a request to the web service.  See 
L<http://developer.tomtom.com/docs/read/geocoding/Request> for fields 
that can be included in HASHREF.  C<key> is your API key.

Returns an object of class Geo::TomTom::Geocoding.

=cut

my $ua = LWP::UserAgent->new;
my $base_uri = 'https://api.tomtom.com/lbs/geocoding/geocode';

sub query {
  my $class = shift;
  my $opt;
  if (ref($_[0]) eq 'HASH') {
    $opt = [ %{ $_[0] } ];
  } elsif (ref($_[0]) eq 'ARRAY') {
    $opt = shift;
  } else {
    $opt = [ @_ ];
  }
  push @$opt, format => 'json';
  my $uri = URI->new($base_uri);
  $uri->query_form($opt);
  warn "Geo::TomTom::Geocoding->request\n$uri\n\n" if $DEBUG;
  my $http_req = HTTP::Request->new(GET => $uri->as_string);
  my $self = $ua->request($http_req);
  bless $self, $class;
  if ( $self->is_success ) {
    my $data = decode_json($self->content);
    if ( $data->{geoResponse}->{count} == 0 ) {
      $self->message("Location not found");
    } else {
      $self->locations( @{$data->{geoResponse}->{geoResult}} );
    }
  }

  $self;
}

=head1 METHODS

=head2 locations

Returns a list of all locations matched by the address query.  Each one
will be a hashref containing any of the elements listed in the documentation:
L<http://developer.tomtom.com/docs/read/geocoding/Response>

=cut

sub locations {
  my $self = shift;
  if ( @_ ) {
    if ( ref($_[0]) eq 'ARRAY' ) {
      $self->{_locations} = shift;
    } elsif ( ref($_[0]) eq 'HASH' ) {
      $self->{_locations} = [ @_ ];
    }
  }
  @{ $self->{_locations} || [] };
}

=head1 AUTHOR

Mark Wells, C<< <mark at freeside.biz> >>

=head1 SUPPORT

You can find documentation for this module with the perldoc command.

    perldoc Geo::TomTom::Geocoding

For the request and response attributes, see the TomTom API documentation:

    L<developer.tomtom.com/docs/read/geocoding>

Commercial support for this module is available from Freeside Internet 
Services:

    L<http://www.freeside.biz/>

=back


=head1 LICENSE AND COPYRIGHT

Copyright (C) 2013 Freeside Internet Services, Inc.

This program is free software; you can redistribute it and/or modify it
under the terms of either: the GNU General Public License as published
by the Free Software Foundation; or the Artistic License.

See http://dev.perl.org/licenses/ for more information.

The data from the TomTom geocoding service is NOT free software.  Consult
TomTom's terms of service for details.

=cut

1; # End of Geo::TomTom::Geocoding
